# Challenge GD4H - Urbasanté

*For an english translated version of this file, follow this [link](/README.en.md)*

Le <a href="https://gd4h.ecologie.gouv.fr/" target="_blank" rel="noreferrer">Green Data for Health</a> (GD4H) est une offre de service incubée au sein de l’ECOLAB, laboratoire d’innovation pour la transition écologique du Commissariat Général au Développement Durable.

Dans ce cadre, un challenge permettant le développement d’outils ancrés dans la communauté de la donnée en santé-environnement afin d’adresser des problématiques partagées a été organisé en 2023.

<a href="https://gd4h.ecologie.gouv.fr/defis" target="_blank" rel="noreferrer">Site</a>

## Urbasanté

En agissant sur nos comportements et nos modes de vies, l’aménagement du territoire a un rôle majeur dans la santé physique, mentale et sociale de la population.

En conséquence, bien que l’espérance de vie en France soit de 86 ans en France, la qualité du tissu urbain peut favoriser les inégalités sociales de santé entre les territoires pouvant atteindre jusqu’à 8 ans d’espérance de vie de différences entre deux quartiers d’une même ville.

<a href="https://gd4h.ecologie.gouv.fr/defis/806" target="_blank" rel="noreferrer">En savoir plus sur le défi</a>


## **Documentation**

A niveau d’exposition identique, les populations plus défavorisées présentent des effets sanitaires plus fréquents, voire plus graves.  Ainsi, dans l’objectif de mieux cibler les actions en urbanisme favorable à la santé et réduire l’exposition des personnes potentiellement sujettes aux inégalités sociales de santé, il est primordial pour les acteurs de l’aménagement de cibler et de mieux comprendre les inégalités sociales présentes dans un quartier.

Avant, il était difficile de comparer les variables socio-économiques de l’Insee et de renseigner les inégalités sociales d’un quartier en comparaison aux autres territoires d’un même bassin de vie, département ou région. Par exemple, ne pas avoir de voiture à Paris n’est pas obligatoirement un indicateur de défaveur sociale, alors que ne pas avoir de voitures en zone rural peut être un indice de défaveur. D’où l’importance de mieux connaître les inégalités sociales dans les zones partageant les mêmes caractéristiques géographiques.

Maintenant, avec UrbaSanté il est possible d’avoir un indicateur de défaveur localisé adapté au bassin de vie, au département, à la région ou encore selon la densité de population. De cette manière les utilisateurs pourront mieux comprendre quelles sont les inégalités sociales locales et ainsi mieux cibler les populations en santé-aménagement. 

<a href="https://urbasante.shinyapps.io/indic_urbasante/" target="_blank" rel="noreferrer">Plateforme développée</a>

<a href="https://gitlab.com/data-challenge-gd4h/urbasante/-/blob/main/docs/note_urbasante.pdf" target="_blank" rel="noreferrer">Note méthodologique</a>

### **Installation**

[Guide d'installation](/INSTALL.md)

### **Utilisation**

Prenons un exemple pour expliciter l'utilisation de l'outil. Un utilisateur souhaite connaître l’indicateur de défaveur d’un Iris de Lyon en comparaison aux autres iris du bassin de vie. 

L’utilisateur sélectionne donc :
 
* Échelle d’analyse : bassin de vie 
* Périmètre géographique : Auvergne Rhônes Alpes 
* Nombres de classes : 8

#### Explication des variables ACP

En cliquant sur l’iris du centre de Montpellier, il est possible de lire l’ensemble des variables socio-économiques sélectionnées par l’ACP intervenant dans l’indice de défaveur. 

* Contribution 

La colonne “contrib” donne la contribution de chaque variable dans l’indice : par exemple l’inégalité socio-économiques ressortant le plus avec les autres iris dans le bassin de vie de Montpellier est la part de propriétaire, la part de résidence en location, la part de chômage, etc.. 
En d’autres termes, ce sont les variables où les inégalités sont les plus importantes dans le bassin de vie de Lyon. 

* Coordonnées 

La colonne “coord” donne la coordonnée de la variable sur l’axe sélectionné de l’ACP. Si la coordonnée est positive alors la variable socio-économique favorise l’indice de défaveur et à l’inverse si la variable est négative alors la variable diminue l’indice de défaveur. 
Ainsi, la colonne “coord” informe l’utilisateur sur la manière dont la variable socio-économique favorise ou non la défaveur. 
Dans notre cas ici,  le chômage et être étranger sont par exemple des variables favorisant l’indice de défaveur. A l’inverse, être propriétaire va diminuer l’indice de défaveur. 

Note : si l’on change l'échelle d’analyse, alors d’autres variables socio-économiques vont apparaître. Elles correspondent aux inégalités socio-économiques propres à l'échelle choisie.

#### Carte

La carte donne une représentation cartographique de l’indice de défaveur en classant les iris du bassin de vie selon 8 classes de défaveur. Plus la couleur est rouge, plus l’iris est dit défavorisé et plus l’iris est clair plus il est dit favorisé selon les variables socio-économiques sélectionnées par l’ACP. 

#### Boîte à moustache

Les boîtes à moustache permettent de représenter d’un point de vue statistique la position de l’iris étudié en comparaison aux autres iris soit du bassin de vie, du département, de la région, de la France entière ou en comparaison aux iris possédant les mêmes densités.. Le point de couleur rouge représente la valeur de l’indice de défaveur de l’iris étudié. Les boîtes à moustaches représentent les valeurs de l’indice de défaveur où sont compris entre 25% et 75% des iris du périmètre d’étude.  Ainsi si le point rouge représentant l’iris d’étude est situé : 
Au-dessus de la boîte à moustache, cela signifie que 75% des iris du périmètre d’études sont dit plus favorisés. 
Au-dessous de la boîte à moustache, cela signifie que 75% des iris du périmètre d’études sont dit plus défavorisés. 

Dans notre cas d’étude de l’iris saint-jean une traduction peut-être : 
"l’iris saint jean est dit plus défavorisé que les 75% des iris situés dans son bassin de vie mais plus favorisé que 75% des iris ayant le même niveau de densité."

### **Limites de l'outil**

#### 1 - Iris sans valeur
Les iris n’ayant pas de données sont ceux où la population présente est faible et/ou des données Insee sont manquantes. Afin que ces iris n’aient pas d’impact sur l’ACP et donc sur la construction de l’indicateur de défaveur, il a été choisi d’exclure ces éléments. 

#### 2 - Coordonnée de l’axe ACP 
Il se peut que pour certaines communes, les coordonnées de certaines variables socio-économiques possèdent un signe pouvant amener à une interprétation opposée. Cela s’explique par l’ACP qui en sélectionnant une dimension va inverser les coordonnées. Ainsi, pour certains territoires, un signe positif indiquera que la variable diminue l’indice de défaveur alors qu’un signe négatif indiquera que la variable favorisera l’indice de défaveur 

#### 3 - Sens des variables 
Certaines variables peuvent avoir un sens opposé selon l’Iris par exemple, la variable "étranger" n'a pas la même signification en banlieue parisienne que dans le 16e arrondissement où il s'agira d'expatriés et de diplomates. 


#### 4 - Légitimité de l’indicateur de défaveur social construit
Les 42 variables socio-économiques de l’Insee ont été choisies car ce sont des marqueurs de défaveurs. Néanmoins, la sélection des variables finales de l’indicateur  par l’ACP ne permettent pas d’affirmer que la combinaison de ces variables sont légitimes pour former un indicateur de défaveur local pertinent. En effet, les variables socio-économiques ressortant de l’ACP sont avant tout celles où la disparité entre iris est la plus importante. 

#### 5 - Contradiction des données 
Pour certains territoires, les données peuvent être contradictoires, il est nécessaire d’avoir une connaissance ou expertise locale permettant de justifier ou expliquer l’apparition de certaines anomalies. 

#### 6 - Poids des variables 

L’ensemble des variables socio-économiques ont le même poids, il serait souhaitable de pondérer ou favoriser l’impact de certaines variables dans la construction de l’indicateur de défaveur.

### **Contributions**

Si vous souhaitez contribuer à ce projet, merci de suivre les [recommendations](/CONTRIBUTING.md).
Voici une [présentation](https://gitlab.com/data-challenge-gd4h/urbasante/-/blob/main/docs/UrbaSante___problemes_pistes_actions.pdf) des problèmes existants et des pistes d'actions associées.

### **Licence**

Le code est publié sous licence [MIT](/licence.MIT).

Les données référencés dans ce README et dans le guide d'installation sont publiés sous [Etalab Licence Ouverte 2.0](/licence.etalab-2.0).
