# Installation Guide

## Data Collection

The main data comes from the Insee website, and the function download.extract.insee() from the file prepare_dataset.R should be updated to use up-to-date data files

Several other data files are needed:
- a geojson file containing IRIS polygons and some other data, including FDep: this file should be located in a folder named 'fdep'
- v_region_2023.csv: the names of French administrative regions, should be located in the root folder
- noms_variables.csv: the labels of the Insee variables used to build the index of social disadvantage

For the map, all RData files should be located in a folder named 'prepared_data'

## Dependencies

This R script has been developped using:
- R version 4.2.1
- tidyverse 1.3.1
- leaflet 2.1.1
- shiny 1.7.4
- sf 1.0-9
- FactoMineR 1.34

## Development

1. run prepare_dataset.R to have all data from Insee downloaded, extracted and prepared
2. run calcul_indicateur_echelles.R to produce all the indexes of social disadvantage
3. run carte_indicateur.R to launch the Shiny app.

