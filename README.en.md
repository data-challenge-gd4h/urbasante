# Challenge GD4H - Urbasanté

<a href="https://gd4h.ecologie.gouv.fr/" target="_blank" rel="noreferrer">Green Data for Health</a> (GD4H) is an initiative led by the Innovation Lab (Ecolab) of the French ministry of ecology.

A challenge has been organised in 2023 to develop tools, rooted in the health-environment data community, aiming at addressing shared issues.
 
<a href="https://gd4h.ecologie.gouv.fr/" target="_blank" rel="noreferrer">Website</a> 

## Urbasanté

By influencing our behavior and lifestyles, regional planning plays a major role in the physical, mental and social health of the population.

As a result, although life expectancy in France is 86 years, the quality of the urban fabric can foster social inequalities in health between areas, with up to 8 years' difference in life expectancy between two districts in the same town.

## **Documentation**

For the same level of exposure, more disadvantaged populations present more frequent and even more serious health effects.  So, in order to better target health-promoting urban planning initiatives and reduce the exposure of people potentially prone to social inequalities in health, it is vital for planners to target and better understand the social inequalities present in a neighborhood.

In the past, it was difficult to compare INSEE socio-economic variables and identify social inequalities in a neighborhood compared to other areas in the same catchment area, département or region. For example, not having a car in Paris is not necessarily an indicator of social disadvantage, whereas not having a car in a rural area can be an indicator of disadvantage. Hence the importance of gaining a better understanding of social inequalities in areas sharing the same geographical characteristics.

Now, with UrbaSanté, it's possible to have a localized indicator of disadvantage adapted to the catchment area, department, region or even population density. In this way, users can gain a better understanding of local social inequalities, and better target health and planning populations.

<a href="https://urbasante.shinyapps.io/indic_urbasante/" target="_blank" rel="noreferrer">Platform developed</a>

<a href="https://gitlab.com/data-challenge-gd4h/urbasante/-/blob/main/docs/note_urbasante.pdf" target="_blank" rel="noreferrer">Methodological note</a>

### **Installation**

[Installation Guide](/INSTALL.md)

### **Usage**

Let's take an example to explain how to use the tool. A user wants to know the disadvantage indicator for an iris in Lyon compared with other irises in the catchment area. 

The user therefore selects : 
* Analysis scale: catchment area 
* Geographic scope: Auvergne Rhônes Alpes 
* Number of classes: 8

#### **Explanation of ACP variables**

By clicking on the iris of the center of Montpellier, it is possible to read all the socio-economic variables selected by the ACP that contribute to the index of disadvantage. 

* Contribution 

The "contrib" column gives the contribution of each variable to the index: for example, the socio-economic inequalities that stand out most with the other iris in the Montpellier catchment area are the share of homeowners, the share of rented housing, the share of unemployment, etc. In other words, these are the variables that contribute most to the index. 
In other words, these are the variables where inequalities are greatest in the Lyon catchment area. 

* Coordinates 

The "coord" column gives the coordinate of the variable on the selected PCA axis. If the coordinate is positive, then the socio-economic variable favors the index of disadvantage; conversely, if the variable is negative, then the variable decreases the index of disadvantage. 
Thus, the "coord" column informs the user about how the socio-economic variable does or does not favor disfavor. 
In our case, unemployment and being a foreigner are, for example, variables that favor the disfavor index. Conversely, being a homeowner will reduce the index of disfavor. 

Note: if we change the scale of analysis, other socio-economic variables will appear. These correspond to the socio-economic inequalities specific to the chosen scale.

#### **Map**

The map gives a cartographic representation of the index of deprivation by classifying the iris of the catchment area according to 8 classes of deprivation. The redder the color, the more disadvantaged the iris, and the lighter the color, the more favored it is according to the socio-economic variables selected by the ACP. 

#### **Mustache box**

Moustache boxes provide a statistical representation of the position of the iris studied in comparison with other irises in the catchment area, department, region or France as a whole, or in comparison with irises with the same density. The red dot represents the value of the disadvantage index for the iris studied. The boxes with whiskers represent the values of the index of unfavorability where between 25% and 75% of the irises in the study perimeter are included. 

So if the red dot representing the study iris is located : 

* Above the moustache box, this means that 75% of irises within the study perimeter are said to be more favored. 
* Below the whisker box, this means that 75% of irises within the study perimeter are said to be more disadvantaged.

In our case study of the iris saint-jean a translation might be:
"the iris saint jean is said to be more disadvantaged than the 75% of iris located in its catchment area but more favored than 75% of iris with the same density level."

### **Tool limitations**

#### 1 - Iris with no value

Iris with no data are those where the population present is small and/or Insee data is missing. To ensure that these irises have no impact on the PCA and therefore on the construction of the disadvantage indicator, we have chosen to exclude these elements. 

#### 2 - PCA axis coordinates 

For some communes, the coordinates of certain socio-economic variables may have a sign that could lead to an opposite interpretation. This is explained by the PCA, which inverts the coordinates by selecting a dimension. Thus, for certain territories, a positive sign indicates that the variable reduces the index of disadvantage, while a negative sign indicates that the variable increases the index of disadvantage. 

#### 3 - Meaning of variables 

For example, the variable "foreigner" does not have the same meaning in the Paris suburbs as in the 16th arrondissement, where it refers to expatriates and diplomats. 

#### 4 - Legitimacy of the constructed social disadvantage indicator

The 42 INSEE socio-economic variables were chosen because they are markers of disadvantage. Nevertheless, the selection of the indicator's final variables by PCA does not allow us to assert that the combination of these variables is legitimate to form a relevant local disadvantage indicator. Indeed, the socio-economic variables that emerge from the PCA are above all those where the disparity between irises is greatest. 

#### 5 - Data contradiction 

For some territories, data may be contradictory, and local knowledge or expertise is needed to justify or explain the appearance of certain anomalies. 

#### 6 - Weight of variables 

All socio-economic variables have the same weight. It would be advisable to weight or favor the impact of certain variables in the construction of the disadvantage indicator.

### **Contributing**

If you wish to conribute to this project, please follow the [recommendations](/CONTRIBUTING.md).

Here's a [presentation](https://gitlab.com/data-challenge-gd4h/urbasante/-/blob/main/docs/UrbaSante___problemes_pistes_actions.pdf) of existing problems and associated courses of action.

### **Licence**

The code is published under [MIT Licence](/LICENSE).

The data referenced in this README and in the installation guide is published under <a href="https://www.etalab.gouv.fr/wp-content/uploads/2018/11/open-licence.pdf">Open Licence 2.0</a>.
